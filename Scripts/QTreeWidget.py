
# Create the GUI window
from PySide2 import QtCore, QtWidgets
from PySide2.QtCore import QStringListModel
from PySide2.QtWidgets import QApplication, QTreeWidget, QTreeWidgetItem
from PySide2.QtGui import QIcon

# Icon and window size
windowWidth = 500
windowHeight = 200

# Create the window
myWin = QtWidgets.QWidget()
myWin.setWindowTitle('My First Window')
myWin.resize(windowWidth, windowHeight)
#myWin.move(100, 100)
#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)

# Add a tree
tree = QTreeWidget(myWin)
tree.setGeometry(QtCore.QRect(0, 0, windowWidth, windowHeight))
tree.setColumnCount(5)

headerList = ['Column A', 'Column B', 'Column C', 'Column D', 'Column E']
tree.setHeaderItem(QTreeWidgetItem(headerList, 1))

cellCount = 10
for i in range(cellCount):
	row = str(cellCount-i).zfill(2)
	rowList = ['A' + row, 'B' + row, 'C' + row, 'D' + row, 'E' + row]
	tree.insertTopLevelItem(0, QTreeWidgetItem(rowList, 1))

# Display the new window
myWin.show()

