
def ButtonClicked():
	print('Button Clicked')

# Create the GUI window
from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QApplication, QPushButton
from PySide2.QtGui import QIcon

# Icon and window size
windowWidth = 400
windowHeight = 50

# Create the window
myWin = QtWidgets.QWidget()
myWin.setWindowTitle('My First Window')
myWin.resize(windowWidth, windowHeight)
#myWin.move(100, 100)
#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)

# Create a button
button = QPushButton(myWin)
button.setText('The Button Label')
button.setGeometry(QtCore.QRect(0, 0, windowWidth, windowHeight))
button.clicked.connect(ButtonClicked)

# Display the new window
myWin.show()

