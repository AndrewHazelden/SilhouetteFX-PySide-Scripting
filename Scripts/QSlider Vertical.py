
def SliderClicked():
	print('[Slider] ' + str(slider.value()) + '\n')

# Create the GUI window
from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QApplication, QSlider
from PySide2.QtGui import QIcon

# Icon and window size
windowWidth = 400
windowHeight = 200

# Create the window
myWin = QtWidgets.QWidget()
myWin.setWindowTitle('My First Window')
myWin.resize(windowWidth, windowHeight)
#myWin.move(100, 100)
#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)

# Add a slider control
slider = QSlider(myWin)
slider.setGeometry(QtCore.QRect(0, 0, windowWidth, windowHeight))
slider.setValue(25)
slider.setMinimum(0)
slider.setMaximum(100)
slider.setOrientation(QtCore.Qt.Orientation(2)) # 1 = Horizontal Orientation/2 = Vertical Orientation
slider.valueChanged.connect(SliderClicked)

# Display the new window
myWin.show()

