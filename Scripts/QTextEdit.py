
def TextChanged():
	print(edit.toPlainText())

# Create the GUI window
from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QApplication, QTextEdit, QPushButton
from PySide2.QtGui import QIcon

# Icon and window size
windowWidth = 400
windowHeight = 50

# Create the window
myWin = QtWidgets.QWidget()
myWin.setWindowTitle('My First Window')
myWin.resize(windowWidth, windowHeight)
#myWin.move(100, 100)
#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)

# Add a TextEdit text field
edit = QTextEdit(myWin)
edit.setGeometry(QtCore.QRect(0, 0, windowWidth, windowHeight))
edit.setPlaceholderText('Please enter a few words.')
edit.setText('Hello Silhouetters!')
edit.textChanged.connect(TextChanged)

# Display the new window
myWin.show()

