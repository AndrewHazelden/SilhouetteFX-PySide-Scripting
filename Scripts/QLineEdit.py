
def PrintButtonClicked():
	print(edit.text())
	
def PrintButtonTextChanged():
	print(edit.text())

# Create the GUI window
from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QApplication, QLineEdit, QPushButton
from PySide2.QtGui import QIcon

# Icon and window size
windowWidth = 400
windowHeight = 50

# Create the window
myWin = QtWidgets.QWidget()
myWin.setWindowTitle('My First Window')
myWin.resize(windowWidth, windowHeight)
#myWin.move(100, 100)
#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)

# Add a LineEdit text field
edit = QLineEdit(myWin)
edit.setGeometry(QtCore.QRect(0, 0, windowWidth, 24))
edit.setPlaceholderText('Please enter a few words.')
edit.setText('Hello Silhouetters!')
edit.textChanged.connect(PrintButtonTextChanged)

# Create a button
button = QPushButton(myWin)
button.setText('Print Text')
button.setGeometry(QtCore.QRect(0, 24, windowWidth, 24))
button.clicked.connect(PrintButtonClicked)

# Display the new window
myWin.show()

