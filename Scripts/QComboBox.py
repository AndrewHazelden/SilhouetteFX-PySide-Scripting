
def ComboBoxClicked():
	#print('[ComboBox] ' + str(box.currentText()) + ' ('+ str(box.currentIndex()) +')\n')
	
	if box.currentIndex() == 0:
		# Apple
		print('[' + str(box.currentText()) + '] Lets make an apple crisp dessert.')
	elif box.currentIndex() == 1:
		# Banana
		print('[' + str(box.currentText()) + '] Lets make a banana split with ice cream.')
	elif box.currentIndex() == 2:
		# Cherry
		print('[' + str(box.currentText()) + '] Lets make some cherry tarts.')
	elif box.currentIndex() == 3:
		# Orange
		print('[' + str(box.currentText()) + '] Lets peel an orange and have sliced orange boats.')
	elif box.currentIndex() == 4:
		# Mango
		print('[' + str(box.currentText()) + '] Lets eat cubed mango chunks with yoghurt.')
	elif box.currentIndex() == 5:
		# Kiwi
		print('[' + str(box.currentText()) + '] Lets have a fresh kiwi snack.')
	else:
		print('[' + str(box.currentText()) + ']')
	
# Create the GUI window
from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QApplication, QComboBox
from PySide2.QtGui import QIcon

# Icon and window size
windowWidth = 400
windowHeight = 200

# Create the window
myWin = QtWidgets.QWidget()
myWin.setWindowTitle('My First Window')
myWin.resize(windowWidth, windowHeight)
#myWin.move(100, 100)
#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)

# Add a ComboBox
box = QComboBox(myWin)
box.setGeometry(QtCore.QRect(0, 0, windowWidth, 24))
box.currentIndexChanged.connect(ComboBoxClicked)

# Add the items to the ComboBox menu
box.addItem('Apple')
box.addItem('Banana')
box.addItem('Cherry')
box.addItem('Orange')
box.addItem('Mango')
box.addItem('Kiwi')

# Display the new window
myWin.show()

