
def ButtonClicked():
	print('Icon Clicked')

# Create the GUI window
from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QApplication, QPushButton
from PySide2.QtGui import QIcon

# Icon and window size
windowWidth = 256
windowHeight = 64

# Create the window
myWin = QtWidgets.QWidget()
myWin.setWindowTitle('My First Window')
myWin.resize(windowWidth, windowHeight)
#myWin.move(100, 100)
#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)

iconFolder = '/Applications/SilhouetteFX/Silhouette v7/Silhouette.app/Contents/Resources/'

# Create a button
button1 = QPushButton(myWin)
button1.setGeometry(QtCore.QRect(64 * 0, 0, 64, 64))
button1.setIcon(QIcon(iconFolder + 'logo.png'))
button1.setFlat(True)
button1.clicked.connect(ButtonClicked)

# Create a button
button2 = QPushButton(myWin)
button2.setGeometry(QtCore.QRect(64 * 1, 0, 64, 64))
button2.setIcon(QIcon(iconFolder + 'logo.png'))
button2.setFlat(True)
button2.clicked.connect(ButtonClicked)

# Create a button
button3 = QPushButton(myWin)
button3.setGeometry(QtCore.QRect(64 * 2, 0, 64, 64))
button3.setIcon(QIcon(iconFolder + 'logo.png'))
button3.setFlat(True)
button3.clicked.connect(ButtonClicked)

# Create a button
button4 = QPushButton(myWin)
button4.setGeometry(QtCore.QRect(64 * 3, 0, 64, 64))
button4.setIcon(QIcon(iconFolder + 'logo.png'))
button4.setFlat(True)
button4.clicked.connect(ButtonClicked)

# Display the new window
myWin.show()

