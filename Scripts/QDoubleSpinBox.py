
def SpinBoxClicked():
	print('[SpinBox] ' + str(box.value()) + '\n')

# Create the GUI window
from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QApplication, QDoubleSpinBox
from PySide2.QtGui import QIcon

# Icon and window size
windowWidth = 400
windowHeight = 200

# Create the window
myWin = QtWidgets.QWidget()
myWin.setWindowTitle('My First Window')
myWin.resize(windowWidth, windowHeight)
#myWin.move(100, 100)
#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)

# Add a checkbox
box = QDoubleSpinBox(myWin)
box.setGeometry(QtCore.QRect(0, 0, windowWidth, 24))
box.valueChanged.connect(SpinBoxClicked)

# Display the new window
myWin.show()

