
def CheckBoxClicked():
	print('[CheckBox] ' + str(box.isChecked()) + '\n')

# Create the GUI window
from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QApplication, QCheckBox
from PySide2.QtGui import QIcon

# Icon and window size
windowWidth = 400
windowHeight = 200

# Create the window
myWin = QtWidgets.QWidget()
myWin.setWindowTitle('My First Window')
myWin.resize(windowWidth, windowHeight)
#myWin.move(100, 100)
#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)

# Add a checkbox
box = QCheckBox(myWin)
box.setGeometry(QtCore.QRect(0, 0, windowWidth, 24))
box.setText('The Checkbox Label')
box.stateChanged.connect(CheckBoxClicked)

# Display the new window
myWin.show()

