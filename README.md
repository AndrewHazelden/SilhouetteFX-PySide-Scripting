# SilhouetteFX PySide Scripting

by Andrew Hazelden &lt;[andrew@andrewhazelden.com](mailto:andrew@andrewhazelden.com)&gt;

## Overview

A collection of small Python code snippets that show hands-on examples of PySide user interface creation with SilhouetteFX 7.x.


# Common PySide GUI Elements

The same code snippets that are listed inline below, are also available as individual .py files that can be found in the "Scripts" folder in this repo.

## QComboBox

![QComboBox](Docs/Images/QComboBox-1.png)

![QComboBox](Docs/Images/QComboBox-2.png)


QComboBox.py:

	def ComboBoxClicked():
		#print('[ComboBox] ' + str(box.currentText()) + ' ('+ str(box.currentIndex()) +')\n')
		
		if box.currentIndex() == 0:
			# Apple
			print('[' + str(box.currentText()) + '] Lets make an apple crisp dessert.')
		elif box.currentIndex() == 1:
			# Banana
			print('[' + str(box.currentText()) + '] Lets make a banana split with ice cream.')
		elif box.currentIndex() == 2:
			# Cherry
			print('[' + str(box.currentText()) + '] Lets make some cherry tarts.')
		elif box.currentIndex() == 3:
			# Orange
			print('[' + str(box.currentText()) + '] Lets peel an orange and have sliced orange boats.')
		elif box.currentIndex() == 4:
			# Mango
			print('[' + str(box.currentText()) + '] Lets eat cubed mango chunks with yoghurt.')
		elif box.currentIndex() == 5:
			# Kiwi
			print('[' + str(box.currentText()) + '] Lets have a fresh kiwi snack.')
		else:
			print('[' + str(box.currentText()) + ']')
		
	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtWidgets import QApplication, QComboBox
	from PySide2.QtGui import QIcon
	
	# Icon and window size
	windowWidth = 400
	windowHeight = 200
	
	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)
	
	# Add a ComboBox
	box = QComboBox(myWin)
	box.setGeometry(QtCore.QRect(0, 0, windowWidth, 24))
	box.currentIndexChanged.connect(ComboBoxClicked)
	
	# Add the items to the ComboBox menu
	box.addItem('Apple')
	box.addItem('Banana')
	box.addItem('Cherry')
	box.addItem('Orange')
	box.addItem('Mango')
	box.addItem('Kiwi')
	
	# Display the new window
	myWin.show()
		



## QCheckBox

![QCheckBox](Docs/Images/QCheckBox-1.png)


QCheckbox.py:

	def CheckBoxClicked():
		print('[CheckBox] ' + str(box.isChecked()) + '\n')

	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtWidgets import QApplication, QCheckBox
	from PySide2.QtGui import QIcon

	# Icon and window size
	windowWidth = 400
	windowHeight = 200

	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)

	# Add a checkbox
	box = QCheckBox(myWin)
	box.setGeometry(QtCore.QRect(0, 0, windowWidth, 24))
	box.setText('The Checkbox Label')
	box.stateChanged.connect(CheckBoxClicked)

	# Display the new window
	myWin.show()



## QDoubleSpinBox

![QDoubleSpinBox](Docs/Images/QDoubleSpinBox.png)

QDoubleSpinBox.py:

		
	def SpinBoxClicked():
		print('[SpinBox] ' + str(box.value()) + '\n')
	
	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtWidgets import QApplication, QDoubleSpinBox
	from PySide2.QtGui import QIcon
	
	# Icon and window size
	windowWidth = 400
	windowHeight = 200
	
	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)
	
	# Add a checkbox
	box = QDoubleSpinBox(myWin)
	box.setGeometry(QtCore.QRect(0, 0, windowWidth, 24))
	box.valueChanged.connect(SpinBoxClicked)
	
	# Display the new window
	myWin.show()
		


## QLabel


![QLabel](Docs/Images/QLabel.png)

QLabel.py:
		
	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtWidgets import QApplication, QLabel
	from PySide2.QtGui import QIcon
	
	# Icon and window size
	windowWidth = 400
	windowHeight = 200
	
	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)
	
	# Add a label
	label = QLabel(myWin)
	label.setGeometry(QtCore.QRect(0, 0, windowWidth, 24))
	label.setText('This is a Label')
	
	# Display the new window
	myWin.show()
	
		

## QLineEdit


![QLineEdit](Docs/Images/QLineEdit.png)

QLineEdit.py:
		
	def PrintButtonClicked():
		print(edit.text())
		
	def PrintButtonTextChanged():
		print(edit.text())
	
	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtWidgets import QApplication, QLineEdit, QPushButton
	from PySide2.QtGui import QIcon
	
	# Icon and window size
	windowWidth = 400
	windowHeight = 50
	
	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)
	
	# Add a LineEdit text field
	edit = QLineEdit(myWin)
	edit.setGeometry(QtCore.QRect(0, 0, windowWidth, 24))
	edit.setPlaceholderText('Please enter a few words.')
	edit.setText('Hello Silhouetters!')
	edit.textChanged.connect(PrintButtonTextChanged)
	
	# Create a button
	button = QPushButton(myWin)
	button.setText('Print Text')
	button.setGeometry(QtCore.QRect(0, 24, windowWidth, 24))
	button.clicked.connect(PrintButtonClicked)
	
	# Display the new window
	myWin.show()
		



## QPushButton


![QPushButton](Docs/Images/QPushButton.png)

QPushButton.py:
		
	def ButtonClicked():
		print('Button Clicked')
	
	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtWidgets import QApplication, QPushButton
	from PySide2.QtGui import QIcon
	
	# Icon and window size
	windowWidth = 400
	windowHeight = 50
	
	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)
	
	# Create a button
	button = QPushButton(myWin)
	button.setText('The Button Label')
	button.setGeometry(QtCore.QRect(0, 0, windowWidth, windowHeight))
	button.clicked.connect(ButtonClicked)
	
	# Display the new window
	myWin.show()



![QPushButton with Icon](Docs/Images/QPushButton-with-Icon.png)


QPushButton with Icon.py:
		
	def ButtonClicked():
		print('Icon Clicked')
	
	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtWidgets import QApplication, QPushButton
	from PySide2.QtGui import QIcon
	
	# Icon and window size
	windowWidth = 256
	windowHeight = 64
	
	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)
	
	iconFolder = '/Applications/SilhouetteFX/Silhouette v7/Silhouette.app/Contents/Resources/'
	
	# Create a button
	button1 = QPushButton(myWin)
	button1.setGeometry(QtCore.QRect(64 * 0, 0, 64, 64))
	button1.setIcon(QIcon(iconFolder + 'logo.png'))
	button1.setFlat(True)
	button1.clicked.connect(ButtonClicked)
	
	# Create a button
	button2 = QPushButton(myWin)
	button2.setGeometry(QtCore.QRect(64 * 1, 0, 64, 64))
	button2.setIcon(QIcon(iconFolder + 'logo.png'))
	button2.setFlat(True)
	button2.clicked.connect(ButtonClicked)
	
	# Create a button
	button3 = QPushButton(myWin)
	button3.setGeometry(QtCore.QRect(64 * 2, 0, 64, 64))
	button3.setIcon(QIcon(iconFolder + 'logo.png'))
	button3.setFlat(True)
	button3.clicked.connect(ButtonClicked)
	
	# Create a button
	button4 = QPushButton(myWin)
	button4.setGeometry(QtCore.QRect(64 * 3, 0, 64, 64))
	button4.setIcon(QIcon(iconFolder + 'logo.png'))
	button4.setFlat(True)
	button4.clicked.connect(ButtonClicked)
	
	# Display the new window
	myWin.show()
		



## QSlider Horizontal

![QSlider Horizontal](Docs/Images/QSlider-Horizontal.png)

QSlider Horizontal.py:

	def SliderClicked():
		print('[Slider] ' + str(slider.value()) + '\n')
	
	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtWidgets import QApplication, QSlider
	from PySide2.QtGui import QIcon
	
	# Icon and window size
	windowWidth = 400
	windowHeight = 200
	
	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)
	
	# Add a slider control
	slider = QSlider(myWin)
	slider.setGeometry(QtCore.QRect(0, 0, windowWidth, windowHeight))
	slider.setValue(25)
	slider.setMinimum(0)
	slider.setMaximum(100)
	slider.setOrientation(QtCore.Qt.Orientation(1)) # 1 = Horizontal Orientation/2 = Vertical Orientation
	slider.valueChanged.connect(SliderClicked)
	
	# Display the new window
	myWin.show()



## QSlider Vertical


![QSlider Vertical](Docs/Images/QSlider-Vertical.png)

QSlider Vertical.py:

	def SliderClicked():
		print('[Slider] ' + str(slider.value()) + '\n')
	
	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtWidgets import QApplication, QSlider
	from PySide2.QtGui import QIcon
	
	# Icon and window size
	windowWidth = 400
	windowHeight = 200
	
	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)
	
	# Add a slider control
	slider = QSlider(myWin)
	slider.setGeometry(QtCore.QRect(0, 0, windowWidth, windowHeight))
	slider.setValue(25)
	slider.setMinimum(0)
	slider.setMaximum(100)
	slider.setOrientation(QtCore.Qt.Orientation(2)) # 1 = Horizontal Orientation/2 = Vertical Orientation
	slider.valueChanged.connect(SliderClicked)
	
	# Display the new window
	myWin.show()



## QSpinBox


![QSpinBox](Docs/Images/QSpinBox.png)

QSpinBox.py:

	def SpinBoxClicked():
		print('[SpinBox] ' + str(box.value()) + '\n')
	
	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtWidgets import QApplication, QSpinBox
	from PySide2.QtGui import QIcon
	
	# Icon and window size
	windowWidth = 400
	windowHeight = 200
	
	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)
	
	# Add a checkbox
	box = QSpinBox(myWin)
	box.setGeometry(QtCore.QRect(0, 0, windowWidth, 24))
	box.valueChanged.connect(SpinBoxClicked)
	
	# Display the new window
	myWin.show()



## QTextEdit


![QTextEdit](Docs/Images/QTextEdit.png)

QTextEdit.py:

	def TextChanged():
		print(edit.toPlainText())
	
	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtWidgets import QApplication, QTextEdit, QPushButton
	from PySide2.QtGui import QIcon
	
	# Icon and window size
	windowWidth = 400
	windowHeight = 50
	
	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)
	
	# Add a TextEdit text field
	edit = QTextEdit(myWin)
	edit.setGeometry(QtCore.QRect(0, 0, windowWidth, windowHeight))
	edit.setPlaceholderText('Please enter a few words.')
	edit.setText('Hello Silhouetters!')
	edit.textChanged.connect(TextChanged)
	
	# Display the new window
	myWin.show()



## QTreeWidget


![QTreeWidget](Docs/Images/QTreeWidget.png)

QTreeWidget.py:

	# Create the GUI window
	from PySide2 import QtCore, QtWidgets
	from PySide2.QtCore import QStringListModel
	from PySide2.QtWidgets import QApplication, QTreeWidget, QTreeWidgetItem
	from PySide2.QtGui import QIcon
	
	# Icon and window size
	windowWidth = 500
	windowHeight = 200
	
	# Create the window
	myWin = QtWidgets.QWidget()
	myWin.setWindowTitle('My First Window')
	myWin.resize(windowWidth, windowHeight)
	#myWin.move(100, 100)
	#myWin.setWindowFlags(QtCore.Qt.FramelessWindowHint)
	
	# Add a tree
	tree = QTreeWidget(myWin)
	tree.setGeometry(QtCore.QRect(0, 0, windowWidth, windowHeight))
	tree.setColumnCount(5)
	
	headerList = ['Column A', 'Column B', 'Column C', 'Column D', 'Column E']
	tree.setHeaderItem(QTreeWidgetItem(headerList, 1))
	
	cellCount = 10
	for i in range(cellCount):
		row = str(cellCount-i).zfill(2)
		rowList = ['A' + row, 'B' + row, 'C' + row, 'D' + row, 'E' + row]
		tree.insertTopLevelItem(0, QTreeWidgetItem(rowList, 1))
	
	# Display the new window
	myWin.show()


Last Edited: 2021-12-06
